cat $1 | sed 's:%:%7$c:g' | sed 's:\t:%2$c:g' | sed 's:        :%2$c:g' | sed 's:":%3$c:g' | tr '\n' '"' | sed 's:":%1$c:g' | sed 's:INSERT_HERE:%4$s:g' | sed 's:CURRENT_NUM:%6$d:g' > quine
cat $1 | sed "s|INSERT_HERE|$(cat quine)|g" | sed "s/CURRENT_NUM/5/g"
