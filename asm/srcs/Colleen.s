section .text
	global main
	extern printf
	default rel
section .data
	format db "section .text%1$c%2$cglobal main%1$c%2$cextern printf%1$c%2$cdefault rel%1$csection .data%1$c%2$cformat db %3$c%4$s%3$c, 0%1$c%1$cfunc:%1$c%2$cret%1$c%1$c%5$c░░░░░░░░▀████▀▄▄░░░░░░░░░░░░░░▄█%1$c%5$c░░░░░░░░░░█▀░░░░▀▀▄▄▄▄▄░░░░▄▄▀▀█%1$c%5$c░░▄░░░░░░░░█░░░░░░░░░░▀▀▀▀▄░░▄▀%1$c%5$c░▄▀░▀▄░░░░░░▀▄░░░░░░░░░░░░░░▀▄▀%1$c%5$c▄▀░░░░█░░░░░█▀░░░▄█▀▄░░░░░░▄█%1$c%5$c▀▄░░░░░▀▄░░█░░░░░▀██▀░░░░░██▄█%1$c%5$c░▀▄░░░░▄▀░█░░░▄██▄░░░▄░░▄░░▀▀░█%1$c%5$c░░█░░▄▀░░█░░░░▀██▀░░░░▀▀░▀▀░░▄▀%1$c%5$c░█░░░█░░█░░░░░░▄▄░░░░░░░░░░░▄▀%1$c   main:%1$c%2$csub rsp, 8%1$c%1$c%2$cmov rsi, 0xA%1$c%2$cmov rdx, 0x9%1$c%2$cmov rcx, 0x22%1$c%2$cmov r8d, format%1$c%2$cmov r9d, 0x3B%1$c%2$clea rdi, [rel format]%1$c%2$cxor rax, rax%1$c%2$ccall printf%1$c%2$ccall func %5$cThis is TOTALLY useful%1$c%1$c%2$cxor rax, rax%1$c%2$cadd rsp, 8%1$c%2$cret%1$c", 0

func:
	ret

;░░░░░░░░▀████▀▄▄░░░░░░░░░░░░░░▄█
;░░░░░░░░░░█▀░░░░▀▀▄▄▄▄▄░░░░▄▄▀▀█
;░░▄░░░░░░░░█░░░░░░░░░░▀▀▀▀▄░░▄▀
;░▄▀░▀▄░░░░░░▀▄░░░░░░░░░░░░░░▀▄▀
;▄▀░░░░█░░░░░█▀░░░▄█▀▄░░░░░░▄█
;▀▄░░░░░▀▄░░█░░░░░▀██▀░░░░░██▄█
;░▀▄░░░░▄▀░█░░░▄██▄░░░▄░░▄░░▀▀░█
;░░█░░▄▀░░█░░░░▀██▀░░░░▀▀░▀▀░░▄▀
;░█░░░█░░█░░░░░░▄▄░░░░░░░░░░░▄▀
   main:
	sub rsp, 8

	mov rsi, 0xA
	mov rdx, 0x9
	mov rcx, 0x22
	mov r8d, format
	mov r9d, 0x3B
	lea rdi, [rel format]
	xor rax, rax
	call printf
	call func ;This is TOTALLY useful

	xor rax, rax
	add rsp, 8
	ret
