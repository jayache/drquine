%assign TOTO 4
%assign TITI TOTO % 2
%define anim anim_ %+ TITI

exec_next:
	xor rax, rax
	mov rdi, final_name
	mov rsi, final_name
	mov rdx, final_name 
	mov r8, 0 
	call execl
	ret
	
fork:
	xor rax, rax
	mov rax, 57
	syscall
	cmp rax, 0
	ret

waitpid_wrapper:
	mov rdi, rax
	mov rsi, 0
	mov rdx, 0
	call waitpid
	ret


sprintf_wrapper:
	mov rdx, TOTO
	cmp rcx, 1
	jz sprintf_wrapper_two
	dec rdx
sprintf_wrapper_two:
	xor rax, rax
	call sprintf
	ret
gcc:
	mov rdi, gcc_path
	mov rsi, gcc_path 
	mov rdx, flag_o
	mov rcx, final_name
	mov r8, gcc_option_3 
	mov r9, object_name 
	mov r10, 0
	push r10
	call execl
	ret

nasm:
	mov rdi, nasm_path
	mov rsi, nasm_path 
	mov rdx, nasm_option_1 
	mov rcx, flag_o 
	mov r8, object_name
	mov r9, source_name
	mov r10, 0
	push r10
	call execl
	ret

set_env:
main:
	mov rdi, quine_name
	xor rax, rax
	call getenv
	mov r14, 1
	cmp rax, 0
	jnz main_after
	mov r14, 0
main_after:
	mov r10, rax
	mov rdi, quine_name
	mov rsi, quine_name 
	mov rcx, 1
	xor rax, rax
	call setenv
	mov rax, r10
	mov rcx, r14
	mov rsi, final_name_pattern
	mov rdi, final_name
	call sprintf_wrapper
	mov rsi, object_name_pattern
	mov rdi, object_name
	mov rcx, r14
	call sprintf_wrapper
	mov rsi, source_name_pattern
	mov rdi, source_name
	mov rcx, r14
	call sprintf_wrapper
	mov rax, 2
	mov rdi, source_name
	mov rsi, 65
	mov rdx, 504
	syscall
	mov rdi, rax
	lea rsi, [rel format]
	mov rdx, 0xA
	mov rcx, 0x9
	mov r8, 0x22
	mov r9, format
	mov r10, 0x3B
	mov r11, TOTO
	cmp r14, 0
	jz main_bis
	dec r11
main_bis:
	mov r12, 0x25
	push r12
	push r11
	push r10
	xor rax, rax
	call dprintf
	xor rax, rax
	pop r10
	pop r11
	pop r12
	mov rdi, anim
	call printf
	xor rax, rax
	mov rax, 3
	mov rbx, rdi
	syscall
	call fork
	jz nasm
	call waitpid_wrapper
	call fork
	jz gcc
	call waitpid_wrapper
	mov rax, TOTO
	cmp rax, 0
	jz exit
	cmp rax, 0
	js exit
	call exec_next
exit:
	ret

section .text
	global main
	extern dprintf
	extern printf
	extern sprintf
	extern waitpid
	extern getenv
	extern setenv
	extern execl
	default rel
section .data
	format db "%7$cassign TOTO %6$d%1$c%7$cassign TITI TOTO %7$c 2%1$c%7$cdefine anim anim_ %7$c+ TITI%1$c%1$cexec_next:%1$c%2$cxor rax, rax%1$c%2$cmov rdi, final_name%1$c%2$cmov rsi, final_name%1$c%2$cmov rdx, final_name %1$c%2$cmov r8, 0 %1$c%2$ccall execl%1$c%2$cret%1$c%2$c%1$cfork:%1$c%2$cxor rax, rax%1$c%2$cmov rax, 57%1$c%2$csyscall%1$c%2$ccmp rax, 0%1$c%2$cret%1$c%1$cwaitpid_wrapper:%1$c%2$cmov rdi, rax%1$c%2$cmov rsi, 0%1$c%2$cmov rdx, 0%1$c%2$ccall waitpid%1$c%2$cret%1$c%1$c%1$csprintf_wrapper:%1$c%2$cmov rdx, TOTO%1$c%2$ccmp rcx, 1%1$c%2$cjz sprintf_wrapper_two%1$c%2$cdec rdx%1$csprintf_wrapper_two:%1$c%2$cxor rax, rax%1$c%2$ccall sprintf%1$c%2$cret%1$cgcc:%1$c%2$cmov rdi, gcc_path%1$c%2$cmov rsi, gcc_path %1$c%2$cmov rdx, flag_o%1$c%2$cmov rcx, final_name%1$c%2$cmov r8, gcc_option_3 %1$c%2$cmov r9, object_name %1$c%2$cmov r10, 0%1$c%2$cpush r10%1$c%2$ccall execl%1$c%2$cret%1$c%1$cnasm:%1$c%2$cmov rdi, nasm_path%1$c%2$cmov rsi, nasm_path %1$c%2$cmov rdx, nasm_option_1 %1$c%2$cmov rcx, flag_o %1$c%2$cmov r8, object_name%1$c%2$cmov r9, source_name%1$c%2$cmov r10, 0%1$c%2$cpush r10%1$c%2$ccall execl%1$c%2$cret%1$c%1$cset_env:%1$cmain:%1$c%2$cmov rdi, quine_name%1$c%2$cxor rax, rax%1$c%2$ccall getenv%1$c%2$cmov r14, 1%1$c%2$ccmp rax, 0%1$c%2$cjnz main_after%1$c%2$cmov r14, 0%1$cmain_after:%1$c%2$cmov r10, rax%1$c%2$cmov rdi, quine_name%1$c%2$cmov rsi, quine_name %1$c%2$cmov rcx, 1%1$c%2$cxor rax, rax%1$c%2$ccall setenv%1$c%2$cmov rax, r10%1$c%2$cmov rcx, r14%1$c%2$cmov rsi, final_name_pattern%1$c%2$cmov rdi, final_name%1$c%2$ccall sprintf_wrapper%1$c%2$cmov rsi, object_name_pattern%1$c%2$cmov rdi, object_name%1$c%2$cmov rcx, r14%1$c%2$ccall sprintf_wrapper%1$c%2$cmov rsi, source_name_pattern%1$c%2$cmov rdi, source_name%1$c%2$cmov rcx, r14%1$c%2$ccall sprintf_wrapper%1$c%2$cmov rax, 2%1$c%2$cmov rdi, source_name%1$c%2$cmov rsi, 65%1$c%2$cmov rdx, 504%1$c%2$csyscall%1$c%2$cmov rdi, rax%1$c%2$clea rsi, [rel format]%1$c%2$cmov rdx, 0xA%1$c%2$cmov rcx, 0x9%1$c%2$cmov r8, 0x22%1$c%2$cmov r9, format%1$c%2$cmov r10, 0x3B%1$c%2$cmov r11, TOTO%1$c%2$ccmp r14, 0%1$c%2$cjz main_bis%1$c%2$cdec r11%1$cmain_bis:%1$c%2$cmov r12, 0x25%1$c%2$cpush r12%1$c%2$cpush r11%1$c%2$cpush r10%1$c%2$cxor rax, rax%1$c%2$ccall dprintf%1$c%2$cxor rax, rax%1$c%2$cpop r10%1$c%2$cpop r11%1$c%2$cpop r12%1$c%2$cmov rdi, anim%1$c%2$ccall printf%1$c%2$cxor rax, rax%1$c%2$cmov rax, 3%1$c%2$cmov rbx, rdi%1$c%2$csyscall%1$c%2$ccall fork%1$c%2$cjz nasm%1$c%2$ccall waitpid_wrapper%1$c%2$ccall fork%1$c%2$cjz gcc%1$c%2$ccall waitpid_wrapper%1$c%2$cmov rax, TOTO%1$c%2$ccmp rax, 0%1$c%2$cjz exit%1$c%2$ccmp rax, 0%1$c%2$cjs exit%1$c%2$ccall exec_next%1$cexit:%1$c%2$cret%1$c%1$csection .text%1$c%2$cglobal main%1$c%2$cextern dprintf%1$c%2$cextern printf%1$c%2$cextern sprintf%1$c%2$cextern waitpid%1$c%2$cextern getenv%1$c%2$cextern setenv%1$c%2$cextern execl%1$c%2$cdefault rel%1$csection .data%1$c%2$cformat db %3$c%4$s%3$c, 0%1$c%2$cflag_o db %3$c-o%3$c, 0%1$c%2$cgcc_path db %3$c/usr/bin/gcc%3$c, 0%1$c%2$cgcc_option_3 db %3$c-no-pie%3$c, 0%1$c%2$cobject_name_pattern db %3$cSully_%7$cd.o%3$c, 0%1$c%2$cobject_name:times 50 db 0%1$c%2$csource_name_pattern db %3$cSully_%7$cd.s%3$c, 0%1$c%2$csource_name:times 50 db 0%1$c%2$cfinal_name_pattern db %3$c./Sully_%7$cd%3$c, 0%1$c%2$cfinal_name:times 50 db 0%1$c%2$cquine_name db %3$cQUINE%3$c, 0%1$c%2$cnasm_path db %3$c/usr/bin/nasm%3$c, 0%1$c%2$cnasm_option_1 db %3$c-felf64%3$c, 0%1$c%2$cprintf_test db %3$c%7$cp%3$c, 0xA, 0%1$c%2$canim_0 db %3$c⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⡞⠛⠻⣦⣀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀%3$c, 0xA, %3$c⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⡁⠀⠈⠁⠀⢹⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀%3$c, 0xA, %3$c⠀⠀⠀⠀⠀⢀⣀⡀⠀⠀⠀⠀⠀⢀⣴⣦⠀⠀⠀⠀⠘⡇⠀⠀⠀⣠⡾⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀%3$c, 0xA, %3$c⠀⠀⢀⡤⢤⡿⠉⠙⣦⠀⠀⠀⣠⣿⣿⣿⠀⠀⠀⠀⠀⢻⡴⠞⠋⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀%3$c, 0xA, %3$c⠀⠀⣿⠁⠀⠁⠀⠀⢸⠀⠀⢰⣿⡿⢟⡿⠀⠀⠀⠀⠀⠀⠀⢀⣠⣴⣶⡄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀%3$c, 0xA, %3$c⠀⠀⠻⣆⠀⠀⠀⠀⣿⠀⠀⡟⠋⠀⢸⠃⢀⣠⠴⢺⡇⢀⠔⠋⢹⣿⡿⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀%3$c, 0xA, %3$c⠀⠀⠀⠈⠙⠶⣄⡘⣿⠀⢸⡇⠀⢀⡿⠚⠉⠀⠀⣼⡷⠁⠀⢀⣾⠟⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀%3$c, 0xA, %3$c⠀⠀⠀⠀⠀⠀⠀⠙⠛⠀⣸⡇⠀⣼⠗⣺⡉⠉⠙⠻⢤⣠⠴⠋⠁⠀⠀⠀⠀⠀⠀⠀⠀⣀⣀⡀⠀⠀⠀%3$c, 0xA, %3$c⠀⠀⠀⠀⠀⠀⠀⠀⣠⠞⠉⣇⣄⠀⠘⠉⠙⠂⢀⡂⠉⠙⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⣼⠋⠈⣻⣄⡀⠀%3$c, 0xA, %3$c⠀⠀⡀⠀⠀⠀⢀⡜⠁⠀⣴⠋⠘⣿⣿⡆⢸⣿⣿⣿⡆⢸⣿⣧⠀⠀⠀⠀⠀⠀⠀⢸⡇⠀⠀⠈⠀⢹⡆%3$c, 0xA, %3$c⠀⠘⠿⢭⣽⣶⠘⠢⡀⠀⣿⠀⠀⠘⡿⠃⠈⣿⣿⣿⠇⠘⠿⣿⠀⠀⠀⠀⠀⠀⠀⠸⡇⠀⠀⠀⣀⡼⠃%3$c, 0xA, %3$c⠀⠀⣤⣖⣻⡿⠀⠀⠈⠢⣿⠀⠀⠀⣷⠀⠀⢻⣿⡟⠀⠀⣰⠷⠶⠒⢶⡄⠀⠀⠀⠀⠛⠛⠛⠋⠉⠀⠀%3$c, 0xA, %3$c⠀⠀⠉⠉⠁⠀⠀⠀⠀⢀⠿⡄⠀⠀⣹⠀⠀⠀⠉⠀⣠⠔⠁⠀⠀⢀⡼⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀%3$c, 0xA, %3$c⠀⣠⣄⣠⠖⠒⣦⠀⢴⣃⠀⢷⠀⠀⠈⠀⠀⠀⠀⠀⠀⠀⢀⣠⡴⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀%3$c, 0xA, %3$c⣾⠁⠀⠋⠀⠀⢸⡇⠀⠈⠙⣾⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠸⢧⣀⣀⠤⣶⠀⠀⢰⣿⣽⠭⠿⠀⠀⠀⠀%3$c, 0xA, %3$c⢿⡀⠀⠀⠀⢀⡾⠃⠀⢠⣾⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡼⠁⠀⡿⠀⠀⠈⠻⣭⣲⡄⠀⠀⠀⠀%3$c, 0xA, %3$c⠈⠳⣄⡀⠀⡾⠁⠀⠀⠈⠉⡿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡼⠁⢀⡼⠁⠀⠀⠀⠀⠀⠈⠁⠀⠀⠀⠀%3$c, 0xA, %3$c⠀⠀⠈⠙⠛⠁⠀⠀⠀⠀⠀⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠸⣇⣠⡞⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀%3$c, 0xA, %3$c⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢧⠀⠀⠀⠀⠀⠀⠀⢀⣠⣤⣤⠴⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀%3$c, 0xA, %3$c⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⣧⣀⣀⣀⣤⠴⠚⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀%3$c, 0xA, %3$c⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡟⠈⠉⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀%3$c, 0xA, %3$c⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣧⢀⣸⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀%3$c, 0xA, %3$c⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠻⠾⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀%3$c, 0xA, 0%1$c%2$canim_1 db %3$c⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡠⣴⡿⠁⠀⠀⠀⠀%3$c, 0xA,%3$c⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⠊⠀⡿⠁⠀⠀⠀⠀⠀%3$c, 0xA,%3$c⠀⠀⢀⣤⣤⡤⠤⠤⠤⢀⣀⡤⠒⠂⠈⠉⠁⠒⢡⡀⡔⠁⠀⠀⠀⠀⠀⠀%3$c, 0xA,%3$c⠀⠀⠀⠉⠛⠿⢦⡀⠀⠀⠀⢀⢄⡀⠀⠀⠀⢠⣲⡙⡄⠀⠀⠀⠀⠀⠀⠀%3$c, 0xA,%3$c⠀⠀⠀⠀⠀⠀⠀⠈⠁⠖⠀⠸⠾⠃⠀⠠⠄⠈⠋⠁⡵⣠⠤⡄⠀⠀⠀⠀%3$c, 0xA,%3$c⠀⠀⠀⠀⠀⠀⢀⡤⣐⡀⠔⠉⢢⠀⠢⠔⠳⡆⠁⠀⢄⠇⢀⠇⠀⠀⠀⠀%3$c, 0xA,%3$c⠀⠀⠀⠀⠀⠀⠻⡀⠈⠑⢌⠒⠊⠀⠀⠁⠈⠀⠀⡠⠊⠀⡘⠀⠀⠀⠀⠀%3$c, 0xA,%3$c⡖⠒⠢⠤⠤⠤⠤⢵⡀⠀⠀⠑⠀⠀⠀⠂⠒⠀⠀⠀⠀⢠⠁⠀⠀⠀⠀⠀%3$c, 0xA,%3$c⡇⠀⠀⠀⠀⠀⠀⠠⠱⡄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⡃⠀⠀⠀⠀⠀⠀%3$c, 0xA,%3$c⡄⠀⠀⣀⡀⠀⣠⡣⡆⡘⠀⢀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡇⠀⠀⠀⠀⠀⠀%3$c, 0xA,%3$c⠉⠉⠁⠀⡆⠀⣩⠠⡗⡇⣸⠟⢣⡀⠀⠀⠀⠀⠀⠀⠀⡇⠀⠀⠀⠀⠀⠀%3$c, 0xA,%3$c⠀⠀⠀⠀⠓⠉⢸⡰⣧⠽⡈⡄⢸⠀⠀⠀⠀⠀⠀⠀⠀⠇⠀⠀⠀⠀⠀⠀%3$c, 0xA,%3$c⠀⠀⠀⠀⠀⠀⠈⠀⠀⠀⠑⠴⢜⣀⠀⠤⢄⣀⠀⢀⠜⠀⠀⠀⠀⠀⠀⠀%3$c, 0xA,%3$c⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢡⠈⡇⠀⠀⠀⠀⠀⠀⠀⠀%3$c, 0xA,%3$c⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢲⠆⠀%3$c, 0xA, 0%1$c", 0
	flag_o db "-o", 0
	gcc_path db "/usr/bin/gcc", 0
	gcc_option_3 db "-no-pie", 0
	object_name_pattern db "Sully_%d.o", 0
	object_name:times 50 db 0
	source_name_pattern db "Sully_%d.s", 0
	source_name:times 50 db 0
	final_name_pattern db "./Sully_%d", 0
	final_name:times 50 db 0
	quine_name db "QUINE", 0
	nasm_path db "/usr/bin/nasm", 0
	nasm_option_1 db "-felf64", 0
	printf_test db "%p", 0xA, 0
	anim_0 db "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⡞⠛⠻⣦⣀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀", 0xA, "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⡁⠀⠈⠁⠀⢹⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀", 0xA, "⠀⠀⠀⠀⠀⢀⣀⡀⠀⠀⠀⠀⠀⢀⣴⣦⠀⠀⠀⠀⠘⡇⠀⠀⠀⣠⡾⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀", 0xA, "⠀⠀⢀⡤⢤⡿⠉⠙⣦⠀⠀⠀⣠⣿⣿⣿⠀⠀⠀⠀⠀⢻⡴⠞⠋⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀", 0xA, "⠀⠀⣿⠁⠀⠁⠀⠀⢸⠀⠀⢰⣿⡿⢟⡿⠀⠀⠀⠀⠀⠀⠀⢀⣠⣴⣶⡄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀", 0xA, "⠀⠀⠻⣆⠀⠀⠀⠀⣿⠀⠀⡟⠋⠀⢸⠃⢀⣠⠴⢺⡇⢀⠔⠋⢹⣿⡿⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀", 0xA, "⠀⠀⠀⠈⠙⠶⣄⡘⣿⠀⢸⡇⠀⢀⡿⠚⠉⠀⠀⣼⡷⠁⠀⢀⣾⠟⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀", 0xA, "⠀⠀⠀⠀⠀⠀⠀⠙⠛⠀⣸⡇⠀⣼⠗⣺⡉⠉⠙⠻⢤⣠⠴⠋⠁⠀⠀⠀⠀⠀⠀⠀⠀⣀⣀⡀⠀⠀⠀", 0xA, "⠀⠀⠀⠀⠀⠀⠀⠀⣠⠞⠉⣇⣄⠀⠘⠉⠙⠂⢀⡂⠉⠙⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⣼⠋⠈⣻⣄⡀⠀", 0xA, "⠀⠀⡀⠀⠀⠀⢀⡜⠁⠀⣴⠋⠘⣿⣿⡆⢸⣿⣿⣿⡆⢸⣿⣧⠀⠀⠀⠀⠀⠀⠀⢸⡇⠀⠀⠈⠀⢹⡆", 0xA, "⠀⠘⠿⢭⣽⣶⠘⠢⡀⠀⣿⠀⠀⠘⡿⠃⠈⣿⣿⣿⠇⠘⠿⣿⠀⠀⠀⠀⠀⠀⠀⠸⡇⠀⠀⠀⣀⡼⠃", 0xA, "⠀⠀⣤⣖⣻⡿⠀⠀⠈⠢⣿⠀⠀⠀⣷⠀⠀⢻⣿⡟⠀⠀⣰⠷⠶⠒⢶⡄⠀⠀⠀⠀⠛⠛⠛⠋⠉⠀⠀", 0xA, "⠀⠀⠉⠉⠁⠀⠀⠀⠀⢀⠿⡄⠀⠀⣹⠀⠀⠀⠉⠀⣠⠔⠁⠀⠀⢀⡼⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀", 0xA, "⠀⣠⣄⣠⠖⠒⣦⠀⢴⣃⠀⢷⠀⠀⠈⠀⠀⠀⠀⠀⠀⠀⢀⣠⡴⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀", 0xA, "⣾⠁⠀⠋⠀⠀⢸⡇⠀⠈⠙⣾⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠸⢧⣀⣀⠤⣶⠀⠀⢰⣿⣽⠭⠿⠀⠀⠀⠀", 0xA, "⢿⡀⠀⠀⠀⢀⡾⠃⠀⢠⣾⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡼⠁⠀⡿⠀⠀⠈⠻⣭⣲⡄⠀⠀⠀⠀", 0xA, "⠈⠳⣄⡀⠀⡾⠁⠀⠀⠈⠉⡿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡼⠁⢀⡼⠁⠀⠀⠀⠀⠀⠈⠁⠀⠀⠀⠀", 0xA, "⠀⠀⠈⠙⠛⠁⠀⠀⠀⠀⠀⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠸⣇⣠⡞⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀", 0xA, "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢧⠀⠀⠀⠀⠀⠀⠀⢀⣠⣤⣤⠴⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀", 0xA, "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⣧⣀⣀⣀⣤⠴⠚⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀", 0xA, "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡟⠈⠉⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀", 0xA, "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣧⢀⣸⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀", 0xA, "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠻⠾⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀", 0xA, 0
	anim_1 db "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡠⣴⡿⠁⠀⠀⠀⠀", 0xA,"⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⠊⠀⡿⠁⠀⠀⠀⠀⠀", 0xA,"⠀⠀⢀⣤⣤⡤⠤⠤⠤⢀⣀⡤⠒⠂⠈⠉⠁⠒⢡⡀⡔⠁⠀⠀⠀⠀⠀⠀", 0xA,"⠀⠀⠀⠉⠛⠿⢦⡀⠀⠀⠀⢀⢄⡀⠀⠀⠀⢠⣲⡙⡄⠀⠀⠀⠀⠀⠀⠀", 0xA,"⠀⠀⠀⠀⠀⠀⠀⠈⠁⠖⠀⠸⠾⠃⠀⠠⠄⠈⠋⠁⡵⣠⠤⡄⠀⠀⠀⠀", 0xA,"⠀⠀⠀⠀⠀⠀⢀⡤⣐⡀⠔⠉⢢⠀⠢⠔⠳⡆⠁⠀⢄⠇⢀⠇⠀⠀⠀⠀", 0xA,"⠀⠀⠀⠀⠀⠀⠻⡀⠈⠑⢌⠒⠊⠀⠀⠁⠈⠀⠀⡠⠊⠀⡘⠀⠀⠀⠀⠀", 0xA,"⡖⠒⠢⠤⠤⠤⠤⢵⡀⠀⠀⠑⠀⠀⠀⠂⠒⠀⠀⠀⠀⢠⠁⠀⠀⠀⠀⠀", 0xA,"⡇⠀⠀⠀⠀⠀⠀⠠⠱⡄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⡃⠀⠀⠀⠀⠀⠀", 0xA,"⡄⠀⠀⣀⡀⠀⣠⡣⡆⡘⠀⢀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡇⠀⠀⠀⠀⠀⠀", 0xA,"⠉⠉⠁⠀⡆⠀⣩⠠⡗⡇⣸⠟⢣⡀⠀⠀⠀⠀⠀⠀⠀⡇⠀⠀⠀⠀⠀⠀", 0xA,"⠀⠀⠀⠀⠓⠉⢸⡰⣧⠽⡈⡄⢸⠀⠀⠀⠀⠀⠀⠀⠀⠇⠀⠀⠀⠀⠀⠀", 0xA,"⠀⠀⠀⠀⠀⠀⠈⠀⠀⠀⠑⠴⢜⣀⠀⠤⢄⣀⠀⢀⠜⠀⠀⠀⠀⠀⠀⠀", 0xA,"⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢡⠈⡇⠀⠀⠀⠀⠀⠀⠀⠀", 0xA,"⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢲⠆⠀", 0xA, 0
