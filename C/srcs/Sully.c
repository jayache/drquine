#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/wait.h>
int main(void) {
	char *s = "#include <stdlib.h>%1$c#include <stdio.h>%1$c#include <unistd.h>%1$c#include <fcntl.h>%1$c#include <string.h>%1$c#include <sys/wait.h>%1$cint main(void) {%1$c%2$cchar *s = %3$c%4$s%3$c;%1$c%2$cint entier = %5$d;%1$c%2$cchar name[20];%1$c%2$cif (getenv(%3$cSully%3$c))%1$c%2$c%2$centier--;%1$c%2$csprintf(name, %3$cSully_%%d.c%3$c, entier);%1$c%2$cint fd = open(name, O_WRONLY | O_CREAT, S_IRWXU);%1$c%2$cchar c = '%6$c%6$c';%1$c%2$cchar n = '%6$cn';%1$c%2$cchar t = '%6$ct';%1$c%2$cchar q = '%3$c';%1$c%2$cdprintf(fd, s, n, t, q, s, entier, c);%1$c%2$c%1$c%2$cclose(fd);%1$c%2$cchar bin_name[10];%1$c%2$cmemcpy(bin_name, name, 8);%1$c%2$cbin_name[7] = 0;%1$c%2$csetenv(%3$cSully%3$c, %3$c1%3$c, 1);%1$c%2$cpid_t pid = fork();%1$c%2$cif (pid == 0) {%1$c%2$c%2$cexecv(%3$c/usr/bin/gcc%3$c, (char*[]){%3$c/usr/bin/gcc%3$c, name, %3$c-o%3$c, bin_name, 0});%1$c%2$c}%1$c%2$cif (entier > 0) {%1$c%2$c%2$cwait(&pid);%1$c%2$c%2$cexecv(bin_name, (char*[]){bin_name, 0});%1$c%2$c}%1$c}%1$c";
	int entier = 5;
	char name[20];
	if (getenv("Sully"))
		entier--;
	sprintf(name, "Sully_%d.c", entier);
	int fd = open(name, O_WRONLY | O_CREAT, S_IRWXU);
	char c = '\\';
	char n = '\n';
	char t = '\t';
	char q = '"';
	dprintf(fd, s, n, t, q, s, entier, c);
	
	close(fd);
	char bin_name[10];
	memcpy(bin_name, name, 8);
	bin_name[7] = 0;
	setenv("Sully", "1", 1);
	pid_t pid = fork();
	if (pid == 0) {
		execv("/usr/bin/gcc", (char*[]){"/usr/bin/gcc", name, "-o", bin_name, 0});
	}
	if (entier > 0) {
		wait(&pid);
		execv(bin_name, (char*[]){bin_name, 0});
	}
}
