#!/bin/bash

set -e
./$1 > tmp_1.c
gcc tmp_1.c -o bin_tmp
./bin_tmp > tmp_2.c

diff tmp_1.c tmp_2.c

rm tmp_1.c tmp_2.c bin_tmp


